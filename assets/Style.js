import { StyleSheet} from 'react-native';

export default StyleSheet.create({
    main: {
        marginTop: 100,
        marginBottom: 100,
    },

    output: {
        textAlign: 'center',
        fontSize: 50,
        marginBottom: 100,
    },

    button1: {
        marginRight: 50,
        marginBottom: 2,
        marginLeft: 50,
        borderRadius: 50,
    },

    button2: {
        marginTop: 50,
        marginRight: 50,
        marginBottom: 2,
        marginLeft: 50,
        borderRadius: 50,
    },

    button3: {
        marginTop: 50,
        marginRight: 50,
        marginBottom: 2,
        marginLeft: 50,
        borderRadius: 50,
    },
});
import React from 'react';
import { StyleSheet, Text, View, WebView } from 'react-native';
import Demo from './assets/demo.html';
import Count from './components/Count';

export default class App extends React.Component {
  render() {
    return (
      <React.Fragment>
      <View style={styles.container}>
        <Text>Press the button to start counting</Text>
      </View>
      <Count/>
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

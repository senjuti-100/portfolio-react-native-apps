import React, { Component } from 'react';
import { Text, View, AppRegistry, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import Style from '../assets/Style';

export default class Count extends Component{
    constructor(props){
        super(props); 
        this.state = {
            count : 0 ,
        };
        //this._onPress = this._onPress.bind(this);
    }

    _onPressIncrement = () => {
        this.setState({
            count: this.state.count + 1
        });
    }

    _onPressDecrement = () => {
        this.setState({
            count: this.state.count - 1
        });
    }

    _onPressReset = () => {
        this.setState({
            count: 0
        });
    }

    render(){
        return(
            <View style={Style.main}>
                <Text style={Style.output}>{this.state.count}</Text>
                <View  style={Style.button1} >
                    <Button title = "Increment" 
                    onPress={this._onPressIncrement} />
                </View>
                <View style={Style.button2} >
                    <Button title="Decrement" 
                    onPress={this._onPressDecrement} /> 
                </View>
                <View style={Style.button3} >
                    <Button title="Reset" 
                    onPress={this._onPressReset} /> 
                </View>
            </View>
        );
    }
}

AppRegistry.registerComponent('count', () => Count);